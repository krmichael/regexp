/*

//ANOTAÇÕES ºº

#João fulano,123.456.789-00,28 de Dezembro de 1991,(21) 3078-2325,Rua do Ouvidor,50,20040-030,Rio de Janeiro


O . em regexp significa qualquer caracter, por isso é preciso usar o escape (para escapar) no exemplo abaixo

CPF: 123.456.789-00
\d{3}\.\d{3}\.\d{3}-\d{2}

O sinal de ? significa um caracter opcional, ou seja, o caracter anterior a ele não é mais obrigátorio,
sendo assim o numero do cpf abaixo também é válido

CPF: 98765432100
\d{3}\.?\d{3}\.?\d{3}-?\d{2}

Os [] define uma classe de caracteres, dentro dela pode ser passado varios  caracteres, ou seja, em caso do
cpf abaixo que recebe um . em vez de - no final, nesse caso eu passo para a classe os dois caracteres que
vai significar um ou outro

CPF: 987.654.321.00
\d{3}\.?\d{3}\.?\d{3}[-.]?\d{2}


[1-3] 1 até 3
\s whitespaces (espaços em branco ou tabs)
\s+ um ou mais espaço em branco | tabs, podendo ser escrito tbem usando classes de quantificadores {1,}

? 0 ou uma vez
+ 1 ou mais vezes
* 0 ou mais vezes

{n} um numero n de vezes
{n,} no minimo n vezes
{n, m} minimo de n+1 e maximo de m

Data: 28 de Dezembro de 1991
[0-3]?\d\s+de\s+[A-Z][a-zç]{3,8}\s+de\s+[12]\d{3}


\d Todos os digitos [0-9]
\s whitespaces [ \t\r\n\f] Espaços, Tabs...
\w wordchar [A-Za-z0-9_] A até Z, a até z, 0 até 9 e underline _ ou (undercore)

********************************************************************************
*                             Âncora                                           *
  Faz uma busca de uma determinada sequência de caracteres em um texto

********************************************************************************

\b word boundary, avalia a expressão antes e depois em busca de wordchar, que são os caracteres de A-Z,
a-z, 0-9 e _, caso encontre um wordchar no inicio ou no final da expressão, essa expressão não é selecionada

ex: aaaa aaa0 aaaa aaa aaaa aaaz aaaa aaa_ aaaa aaaZ
\baaa\b seleciona os 3 'as' seguidos, caso eles não tenha nenhum wordchar no inicio ou no fim 

O sinal de ^ significa inicio
O sinal de . significa qualquer coisa (qualquer caracter)
O sinal de + significa 1 ou mais caracter
o Sinal de $ significa no final

file:///home/ls/Cursos/Regex/regex/index.html

^file.+\.html$ (Url com final .html)

^file.+[.html|.htm]$ (Url com final .html ou .htm)


#Grupos (), usado para extrair uma informação dentro de uma expressão

No exemplo abaixo os () significa um grupo que nesse caso foi definido para extrair o ano

[0-3]?\d\s+de\s+[A-Z][a-zç]{3,8}\s+de\s+([12]\d{3})

Tornando a preposição 'de' e os espaços opcional, criando um grupo para eles

[0-3]?\d\s+(de\s+)?[A-Z][a-zç]{3,8}\s+(de\s+)?([12]\d{3})

Não mostrando o resultado de um determinado grupo, ex: a preposição 'de' e seus espaços caso eles exista
não é mostrado no resultado final, para isso usa-se ?: antes do grupo que precisa fica oculto

?: Non-capturing-group

[0-3]?\d\s+(?:de\s+)?[A-Z][a-zç]{3,8}\s+(?:de\s+)?([12]\d{3})

Grupos para Dia, Mês, Ano

([0-3]?\d)\s+(?:de\s+)?([A-Z][a-zç]{3,8})\s+(?:de\s+)?([12]\d{3})


#Quantifiers

O sinal de ? também serve como stop em uma determinada expressão

ex: <h1 class="titulo">Expressões regulares</h1>

<h1.+?> Seleciona a tag h1, qualquer caracter depois dele, podendo ser um ou mais, porem pare quando encontrar
o sinal de >

<h1 class="titulo">Expressões regulares</h1>

<h1.+?>([\s\wõ]+)</h1>
ou seja, depois do sinal de > temos uma classe onde foi definido um wordchar (A-Za-z0-9_), 1 ou + wordchar
passando támbem caracter com acentos (Não são abrangidos pelo wordchar) e um \s no inicio para indicar
que o conteudo pode ter espaços, por final os () para definir o grupo (que contém apenas o contéudo)


#Backreferences, referencia um grupo definido anteriormente

ex: <h1 class="titulo">Expressões regulares</h1>

<(h1|h2).+?>([\s\wõ]+)</\1>
O 1 refere-se ao primeiro grupo (h1|h2) da expressão, dessa forma se a tag for iniciada com h1 o 1 que se
refere a esse grupo vai se tornar o h1, caso contrario a 1 se torna h2...
*/